var assert = require("assert");
var React = require('react');
var TestUtils = require('react/lib/ReactTestUtils');
var expect = require('expect');
var LoginBox = require('../src/acme-login-box');

describe('acme-login-box component test suite', function () {
  it('loads without problems', function () {
    assert.notEqual(undefined, LoginBox);
  });

  it('renders into document', function() {
    var root = TestUtils.renderIntoDocument(<LoginBox />);
    expect(root).toExist();
  });
});
