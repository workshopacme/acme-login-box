import React from 'react';
import {TransformInput} from 'sui-transform-input';

const transform = function(field) {
  return function(value) {
    const result = value.toUpperCase();
    this[field] = result;
    return result;
  };
};

export default class LoginBox extends React.Component {
  constructor(props) {
    super(props);
    this.username = '';
    this.password = '';
  }
  handleClick() {
    this.props.onLogin({username: this.username, password: this.password});
  }
  render() {
    return (
      <div className='acme-login-box'>
        <TransformInput transform={transform('username').bind(this)}/>
        <TransformInput transform={transform('password').bind(this)}/>
        <button className='acme-login-box__button' onClick={this.handleClick.bind(this)}>Login</button>
      </div>
    );
  }
}

LoginBox.propTypes = {onLogin: React.PropTypes.func};
